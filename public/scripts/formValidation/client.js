$(document).ready(function() {
	$("#clientForm").form({
		fields: {
			"client[name]": {
				identifier: "client[name]",
				rules: [
					{
						type: "empty",
						prompt: "Client name cannot be empty"
					}
				]
			},
			"client[email]": {
				identifier: "client[email]",
				optional: true,
				rules: [
					{
						type: "email",
						prompt: "Client email needs to be a valid email"
					}
				]
			}
		}
	});
});
