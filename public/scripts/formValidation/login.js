$(document).ready(function() {
	$(".ui.form").form({
		fields: {
			username: {
				identifier: "username",
				rules: [
					{
						type: "empty",
						prompt: "Please enter your username"
					}
				]
			},
			password: {
				identifier: "password",
				rules: [
					{
						type: "empty",
						prompt: "Please enter your password"
					},
					{
						type: "length[5]",
						prompt: "Your password must be at least 5 characters."
					}
				]
			}
		}
	});
});
