const showOutputs = {};

window.$(document).ready(function() {
	window.$("#editOutputBtn").click(showOutputs.outputEdit);
	window.$("#updateOutputBtn").click(showOutputs.outputUpdate);
	window.$("#cancelOutputBtn").click(showOutputs.outputCancel);
});

showOutputs.outputEdit = function() {
	showOutputs.formData = window.$("#outputForm").serializeArray();

	window.$("#editOutputBtn").addClass("disabled");
	window.$(".outputEditing").removeClass("disabled");
	window.$(".outputReadonly").prop("readonly", false);
};

showOutputs.outputUpdate = function() {
	window.$(".dimmer").dimmer("show");
	window.$.ajax({
		url: "/api/batches/" + window.$("input#batchID").val() + "/outputs",
		type: "PUT",
		data: window.$("#outputForm").serializeArray(),
		success: function(data, status) {
			if (data.hasOwnProperty("err")) {
				window.toastr.error(data.err, "Saving the outputs failed.");
			} else {
				window.$("#editOutputBtn").removeClass("disabled");
				window.$(".outputEditing").addClass("disabled");
				window.$(".outputReadonly").prop("readonly", true);
				window.toastr.success("Your changes to the outputs have been saved.", "Changes saved");
			}
			window.$(".dimmer").dimmer("hide");
		}
	});
};

showOutputs.outputCancel = function() {
	window.$("#editOutputBtn").removeClass("disabled");
	window.$(".outputEditing").addClass("disabled");
	window.$(".outputReadonly").prop("readonly", true);

	window.restoreForm("#outputForm", showOutputs.formData);
};
