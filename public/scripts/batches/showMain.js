const showMain = {};

window.$(document).ready(function() {
	window.$("#batchForm").form({
		fields: {
			clientSelect: "empty",
			typeSelect: "empty",
			cultureInput: "empty",
			speciesInput: "empty"
		}
	});

	window.$(".dropdown").dropdown();

	window.$("#newClientBtn").click(function() {
		window.$(".ui.modal").modal("show");
	});

	window.$("#deleteMainBtn").click(function() {
		window.$("#deleteModal").modal("show");
	});
	window.$("#editMainBtn").click(showMain.mainEdit);
	window.$("#updateMainBtn").click(showMain.mainUpdate);
	window.$("#cancelMainBtn").click(showMain.mainCancel);
});

showMain.mainEdit = function() {
	showMain.formData = window.$("#mainForm").serializeArray();

	window.$("#editMainBtn").addClass("disabled");
	window.$(".mainEditing").removeClass("disabled");

	window.$("#newClientBtn").transition("fade up");
	window.$("#mainForm .dropdown").removeClass("disabled");
	window.$(".mainReadonly").prop("readonly", false);
};

showMain.mainUpdate = function() {
	if (window.$("#mainForm").form("validate form")) {
		window.$(".dimmer").dimmer("show");
		window.$.ajax({
			url: "/api/batches/" + window.$("input#batchID").val(),
			type: "PUT",
			data: window.$("#mainForm").serializeArray(),
			success: function(data, status) {
				if (data.hasOwnProperty("err")) {
					window.toastr.error(data.err, "Saving the batch failed");
				} else {
					window.$("#editMainBtn").removeClass("disabled");
					window.$(".mainEditing").addClass("disabled");

					window.$("#newClientBtn").transition("fade up");
					window.$("#mainForm .dropdown").addClass("disabled");
					window.$(".mainReadonly").prop("readonly", true);

					window.toastr.success("Your changes have been saved into the database.", "Changes saved");
				}
				window.$(".dimmer").dimmer("hide");
			}
		});
	}
};

showMain.mainCancel = function() {
	window.$("#editMainBtn").removeClass("disabled");
	window.$(".mainEditing").addClass("disabled");

	window.$("#newClientBtn").transition("fade up");
	window.$("#mainForm .dropdown").addClass("disabled");
	window.$(".mainReadonly").prop("readonly", true);

	window.restoreForm("#mainForm", showMain.formData);
	window.$("#clientDropdown").dropdown("set selected", showMain.formData[1].value);
	window.$("#typeDropdown").dropdown("set selected", showMain.formData[2].value);
};
