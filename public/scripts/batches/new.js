const newForm = {
	bagId: 0
};

$(document).ready(function() {
	// TODO Cancel button
	$("#clientDropdown").dropdown();
	$("#typeDropdown").dropdown();

	$("#newClientBtn").click(function() {
		$(".ui.modal").modal("show");
	});

	$("#newBagBtn").click(addBagSlot);

	// Sets a click function on all elements with removeBag class
	$(document).on("click", ".removeBag", function() {
		$("#bag" + $(this).attr("value")).remove();
	});

	$(document).on("keydown", ".lastBagField", function(e) {
		if (event.which == 9) {
			addBagSlot();
		}
	});

	$("#batchForm").form({
		fields: {
			clientSelect: "empty",
			typeSelect: "empty",
			cultureInput: "empty",
			speciesInput: "empty"
		}
	});
});

function addBagSlot() {
	let html = `
	<div id="bag<%id%>">
		<div class="three fields">
			<div class="seven wide field">
				<label>Number</label>
				<input type="number" name="batch[bags][<%id%>][number]">
			</div>
			<div class="seven wide field">
				<label>Weight</label>
				<input type="number" class="lastBagField" name="batch[bags][<%id%>][weight]">
			</div>
			<div class="two wide field">
				<label>&nbsp;</label>
				<div class="ui tiny icon red button removeBag" value="<%id%>">
					<i class="minus icon"></i>
				</div>
			</div>
		</div>
	</div>
	`;

	$("#bagForm").append(html.replace(/<%id%>/gi, newForm.bagId));
	newForm.bagId++;

	// From header.ejs
	stickNavBar();
}
