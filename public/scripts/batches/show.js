const showData = {
	editingBags: false,
	bagId: 0,

	outputFormData: {},
	bagsFormData: {},
	bagsFormHTML: ""
};

$(document).ready(function() {
	$(".dimmer").dimmer({
		closable: false
	});
	$("#deleteModal").modal({
		onApprove: function() {
			$("#deleteForm").submit();
			return true;
		}
	});
});


function restoreForm(form, formArray) {
	formArray.forEach(function(pair) {
		let selector = `input[name="${ pair.name }"], textarea[name="${ pair.name }"]`;
		let input = $(form).find(selector);
		input.val(pair.value);
	});
}
