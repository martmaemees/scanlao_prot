const showBags = {
	bagId: 0
};

window.$(document).ready(function() {
	window.$(document).on("click", "#editBagsBtn", showBags.bagsEdit);
	window.$(document).on("click", "#updateBagsBtn", showBags.bagsUpdate);
	window.$(document).on("click", "#cancelBagsBtn", showBags.bagsCancel);
	window.$(document).on("click", "#newBagBtn", showBags.addBagSlot);

	window.$(document).on("click", ".removeBag", function() {
		window.$("#bag" + window.$(this).attr("value")).remove();
	});

	window.$(".lastBagField").not(":last").removeClass("lastBagField");
	window.$(document).on("keydown", ".lastBagField", function(e) {
		if (event.which == 9) {
			showBags.addBagSlot();
		}
	});
});

showBags.bagsEdit = function() {
	showBags.formHTML = window.$("#bagForm").html();
	showBags.formData = window.$("#bagForm").serializeArray();

	window.$("#editBagsBtn").addClass("disabled");
	window.$(".bagsEditing").removeClass("disabled");
	window.$(".bagReadonly").prop("readonly", false);
};

showBags.bagsUpdate = function() {
	window.$(".dimmer").dimmer("show");
	window.$.ajax({
		url: "/api/batches/" + window.$("input#batchID").val() + "/bags",
		type: "PUT",
		data: window.$("#bagForm").serializeArray(),
		success: function(data, status) {
			if (data.hasOwnProperty("err")) {
				window.toastr.error(data.err, "Saving the bags failed");
			} else {
				window.$("#editBagsBtn").removeClass("disabled");
				window.$(".bagsEditing").addClass("disabled");
				window.$(".bagReadonly").prop("readonly", true);

				data.bags.forEach((bag) => {
					console.log(bag);
					window.$("#bag" + bag._id + " #bagNrField").val(bag.number);
					window.$("#bag" + bag._id + " #bagWeightField").val(bag.weight);
				});

				window.toastr.success("Your changes to the bags have been saved.", "Changes saved");
			}
			window.$(".dimmer").dimmer("hide");
		}
	});
};

showBags.bagsCancel = function() {
	window.$("#editBagsBtn").removeClass("disabled");
	window.$(".bagsEditing").addClass("disabled");
	window.$(".bagReadonly").prop("readonly", true);

	window.$("#bagForm").html(showBags.formHTML);
	window.restoreForm("#bagForm", showBags.formData);
};

showBags.addBagSlot = function() {
	let html = `
	<div id="bag<%id%>" class="bagSlot">
		<div class="three fields">
			<div class="seven wide field">
				<label>Number</label>
				<input class="bagReadonly" type="number" name="bags[<%id%>][number]">
			</div>
			<div class="seven wide field">
				<label>Weight</label>
				<input class="lastBagField bagReadonly" type="number" name="bags[<%id%>][weight]">
			</div>
			<div class="two wide field">
				<label>&nbsp;</label>
				<div class="bagsEditing ui tiny icon red button removeBag" value="<%id%>">
					<i class="minus icon"></i>
				</div>
			</div>
		</div>
	</div>
	`;

	window.$(".lastBagField").removeClass("lastBagField");
	window.$("#bagForm").append(html.replace(/<%id%>/gi, showBags.bagId));
	showBags.bagId++;

	// From header.ejs
	window.stickNavBar();
};
