$(document).ready(function() {
	$("#deleteModal").modal({
		onApprove: function() {
			$("#deleteForm").submit();
			return true;
		}
	});
	$("#deleteButton").click(function() {
		$("#deleteModal").modal("show");
	});
});
