$(document).ready(function() {
	// For some reason form validation doesn't work when doing this.
	// $("#clientForm input").keypress(function(e) {
	// 	if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
	// 		console.log("APPROVE");
	// 		clientModalApprove();
	// 	}
	// });

	$(".ui.modal").modal({
		onApprove: clientModalApprove
	});

	$("#clientForm").form({
		fields: {
			clientName: {
				identifier: "client[name]",
				rules: [
					{
						type: "empty",
						prompt: "Client name cannot be empty"
					}
				]
			},
			clientEmail: {
				identifier: "client[email]",
				optional: true,
				rules: [
					{
						type: "email",
						prompt: "Client email needs to be a valid email"
					}
				]
			}
		}
	});
});

function clientModalApprove() {
	if ($("#clientForm").form("validate form")) {
		$.post("/api/clients", $(".modal form").serializeArray(), function(data, status) {
			if (data.hasOwnProperty("err")) {
				toastr.error(data.err, "Client creation failed");
			} else {
				// Add the new client to the clients dropdown
				let newClient = document.createElement("div");
				newClient.setAttribute("data-value", data._id);
				newClient.classList.add("item");
				newClient.innerText = data.name;
				$("#clientMenu").append(newClient);

				$(".ui.modal").modal("hide");
				toastr.success("Created a new client: " + data.name, "Client created");
				$("#clientForm")[0].reset();
			}
		});
	}
	return false;
}
