#!/bin/sh

# Initialize the env.json config file
if [ ! -f ./config/env.json ]; then
	echo "Didn't find environment variables file, creating one from defaults."
	cp ./config/env.example.json ./config/env.json;
else
	echo "env.json config file exists, not taking action."
fi

# Install npm dependencies
npm install
