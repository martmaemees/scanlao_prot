const express = require("express");
const path = require("path");
const favicon = require("serve-favicon");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const passport = require("passport");
const LocalStrategy = require("passport-local");
const methodOverride = require("method-override");
const flash = require("connect-flash");
const i18n = require("i18n-express");
const helmet = require("helmet");
const debug = require("debug")("debug");

const envConf = require("./config/env.json");

// ROUTES
const index = require("./routes/index");
const auth = require("./routes/auth");
const batches = require("./routes/batches");
const clients = require("./routes/clients");

const batchAPI = require("./routes/api/batches");
const clientAPI = require("./routes/api/clients");

// MODELS
const User = require("./models/user");

const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.set("trust proxy", 1);
app.use(require("express-session")({
	secret: envConf.SESSION_SECRET,
	resave: false,
	saveUninitialized: false,
	name: envConf.SESSION_NAME,
	cookie: {
		secure: envConf.COOKIE_SECURE,
		httpOnly: true,
		domain: envConf.COOKIE_DOMAIN,
		path: envConf.COOKIE_PATH,
		maxAge: 1000 * 60 * 60 * 24 // Max age for 24 hours.
	}
}));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(methodOverride("_method"));
app.use(flash());
app.use(express.static(path.join(__dirname, "public")));
app.use(i18n({
	translationsPath: path.join(__dirname, "i18n"),
	browserEnable: false,
	defaultLang: "en",
	siteLangs: ["en", "et"],
	textsVarName: "trans",
	paramLangName: "clang",
	cookieLangName: "ulang"
}));
app.use(helmet());

// PASSPORT CONF
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// if (process.env.NODE_ENV == "production") {
// 	mongoose.connect(process.env.DB_URI);
// 	console.log("Connecting to Mongoose server on: " + process.env.DB_URI);
// } else {
// 	mongoose.connect("mongodb://localhost/scanlao");
// 	console.log("Connecting to Mongoose server on: mongodb://localhost/scanlao");
// }
console.log("Connecting to database on: " + envConf.DB_URI);
mongoose.connect(envConf.DB_URI).then(
	() => {
		debug("Mongoose is connected.");
	},
	(err) => {
		debug("Mongoose connection failed");
		debug(err);
	}
);

// SEEDING
const seeds = require("./seeds");
seeds();

// MIDDLEWARES
app.use(function(req, res, next) {
	res.locals.user = req.user;
	res.locals.error = req.flash("error");
	res.locals.success = req.flash("success");
	next();
});

// ROUTES
app.use("/", index);
app.use("/", auth);
app.use("/batches", batches);
app.use("/clients", clients);

app.use("/api/batches", batchAPI);
app.use("/api/clients", clientAPI);

// ERROR HANDLING
// catch 404 and forward to error handler
app.use(function(req, res, next) {
	let err = new Error("Not Found");
	err.status = 404;
	next(err);
});

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render("error");
});

module.exports = app;
