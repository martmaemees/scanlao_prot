// CLIENT ROUTES
// Route prefix: /clients

const express = require("express");
const router = express.Router();

const Client = require("../models/client");
const Batch = require("../models/batch");

const authMW = require("../middleware/auth");

router.use(authMW.isLoggedIn);

const pageSize = 15;

// INDEX
router.get("/", async function(req, res) {
	const searchTerms = {};
	let page = 1;
	if (req.query.name) {
		searchTerms.name = new RegExp(req.query.name, "i");
	}
	if (req.query.email) {
		searchTerms.email = new RegExp(req.query.email, "i");
	}
	if (req.query.phoneNumber) {
		searchTerms.phoneNumber = new RegExp(req.query.phoneNumber, "i");
	}
	if (req.query.page) {
		page = req.query.page;
	}

	const clients = await Client.find(searchTerms).sort("-createdAt").skip(pageSize *(page-1)).limit(pageSize).exec().catch(function(err) {
		console.log("Failed to get clients:");
		console.log(err);
		req.flash("error", "Failed to get the clients from the database.");
	});
	const clientCount = await Client.count(searchTerms);
	res.render("clients/index", {
		title: "Clients | ScanLao",
		clients: clients,
		search: req.query,
		pageCount: Math.ceil(clientCount/pageSize),
		page: page
	});
});

// NEW
router.get("/new", function(req, res) {
	res.render("clients/new", {
		title: "New Client | ScanLao"
	});
});

// CREATE
router.post("/", function(req, res) {
	req.body.client.author = req.user;
	Client.create(req.body.client, function(err, client) {
		if (err || !client) {
			console.log(err);
			req.flash("error", "Something went wrong when creating the client.");
			return res.redirect("back");
		}
		res.redirect("/clients");
	});
});

// SHOW
router.get("/:id", function(req, res) {
	Client.findById(req.params.id, async function(err, client) {
		if (err || !client) {
			console.log(err);
			req.flash("error", "Failed to find that client.");
			return res.redirect("back");
		}
		const batches = await Batch.find({client: req.params.id});
		console.log(batches);
		res.render("clients/show", {
			title: "Client | ScanLao",
			client: client,
			batches: batches
		});
	});
});

router.get("/:id/edit", function(req, res) {
	Client.findById(req.params.id, function(err, client) {
		if (err || !client) {
			console.log(err);
			req.flash("error", "Failed to find that client.");
			return res.redirect("back");
		}
		res.render("clients/edit", {
			title: "Edit Client | ScanLao",
			client: client
		});
	});
});

// UPDATE
router.put("/:id", function(req, res) {
	Client.findByIdAndUpdate(req.params.id, req.body.client, function(err, updatedClient) {
		if (err || !updatedClient) {
			console.log(err);
			req.flash("error", "Something went wrong when updating the client");
			return res.redirect("back");
		}
		res.redirect("/clients/" + req.params.id);
	});
});

// DESTROY
router.delete("/:id", async function(req, res) {
	if (await Batch.count({client: req.params.id}) === 0) {
		Client.delete({_id: req.params.id}, function(err) {
			if (err) {
				console.log(err);
				req.flash("error", "Something went wrong when deleting the client.");
				return res.redirect("back");
			}
			res.redirect("/clients");
		});
	} else {
		req.flash("error", "Cannot delete a client when it has batches tied to it.");
		res.redirect("/clients/" + req.params.id);
	}
});

module.exports = router;
