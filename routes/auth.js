// AUTHENTICATION ROUTES
// Route prefix: "/"
const express = require("express");
const router = express.Router();
const passport = require("passport");

const User = require("../models/user");

router.get("/login", function(req, res) {
	if (req.isAuthenticated()) {
		req.flash("error", "You are already logged in.");
		return res.redirect("/");
	}
	res.render("auth/login", {title: "ScanLao - Login", noNav: true});
});

router.post("/login", passport.authenticate("local", {
	successRedirect: "/",
	failureRedirect: "/login"
}), function(req, res) {});

router.get("/logout", function(req, res) {
	if (!req.isAuthenticated()) {
		req.flash("error", "You need to be logged in to do that");
		return res.redirect("back");
	}
	req.logout();
	req.flash("success", "You have logged out.");
	res.redirect("/");
});

module.exports = router;
