// BATCHE ROUTES
// Route prefix: /batches

const express = require("express");
const router = express.Router();
// const passport = require("passport");

const dataset = require("../config/dataset.json");

const Batch = require("../models/batch");
const Client = require("../models/client");
const Bag = require("../models/bag");

const authMW = require("../middleware/auth");

const pageSize = 15;

router.use(authMW.isLoggedIn);

// INDEX
router.get("/", async function(req, res) {
	const searchTerms = {};
	let page = 1;
	if (req.query.type) {
		searchTerms.type = req.query.type;
	}
	if (req.query.culture) {
		searchTerms.culture = new RegExp(req.query.culture, "i");
	}
	if (req.query.species) {
		searchTerms.species = new RegExp(req.query.species, "i");
	}
	if (req.query.page) {
		page = req.query.page;
	}

	// sort("-param") Descending sort.
	const batches = await Batch.find(searchTerms).sort("-createdAt").skip(pageSize * (page-1)).limit(pageSize).populate("client").exec().catch(function(err) {
		console.log("Failed to get the batches");
		console.log(err);
		req.flash("error", "Failed to get the batches.");
	});
	const batchCount = await Batch.count(searchTerms);
	res.render("batches/index", {
		title: "Batches | ScanLao",
		batches: batches,
		types: dataset.batchTypes,
		search: req.query,
		pageCount: Math.ceil(batchCount / pageSize),
		page: page,
	});
});

// NEW
router.get("/new", async function(req, res) {
	Client.find({}).sort("-createdAt").exec(function(err, clients) {
		if (err) {
			console.log(err);
			req.flash("error", "Something went wrong when fetching clients.");
			return res.redirect("/batches");
		}
		res.render("batches/new", {
			title: "New Batch | ScanLao",
			clients: clients,
			types: dataset.batchTypes
		});
	});
});

// CREATE
router.post("/", function(req, res) {
	// return res.send(req.body.batch);
	req.body.batch.author = req.user;
	Batch.create(req.body.batch, function(err, batch) {
		if (err) {
			console.log(err);
			req.flash("error", err);
			return res.redirect("back");
		}
		res.redirect("/batches");
	});
});

// SHOW
router.get("/:id", async function(req, res) {
	Batch.findById(req.params.id).populate("client").exec(function(err, batch) {
		if (err || !batch) {
			console.log(err);
			req.flash("error", "Couldn't find the specified batch.");
			return res.redirect("/batches");
		}
		Client.find({}).sort("-createdAt").exec(function(err, clients) {
			if (err) {
				console.log(err);
				req.flash("error", "Failed to fetch the clients list.");
			}
			res.render("batches/show", {
				title: "Batch | ScanLao",
				batch: batch,
				clients: clients,
				types: dataset.batchTypes
			});
		});
	});
});

// DESTROY
router.delete("/:id", function(req, res) {
	Batch.delete({_id: req.params.id}, function(err, batch) {
		if (err) {
			console.log(err);
			req.flash("error", "Something went wrong when deleting the batch.");
		}
		res.redirect("/batches");
	});
});

module.exports = router;
