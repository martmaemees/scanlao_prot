// CLIENT API ROUTES
// Route prefix: /api/clients

const express = require("express");
const router = express.Router();
const debug = require("debug")("debug");

const Batch = require("../../models/batch");
const Bag = require("../../models/bag");
const Client = require("../../models/client");

const authMW = require("../../middleware/auth");

router.use(authMW.isLoggedIn);

// UPDATE main
router.put("/:id", function(req, res) {
	console.log(req.body.batch);
	Batch.findByIdAndUpdate(req.params.id, req.body.batch, function(err, batch) {
		if (err || !batch) {
			console.log("Error updating a batch through api:\n" + err);
			res.send({err: "Failed to update the batch data."});
		} else {
			res.status(200).send(batch);
		}
	});
});

// UPDATE bags
router.put("/:id/bags", async function(req, res) {
	const batch = await Batch.findById(req.params.id).exec().catch((err) => {
		console.log(err);
		res.send({err: "Failed to find the batch."});
	});
	if (!batch) {
		res.send({err: "Failed to find the batch."});
		return;
	}

	// console.log(batch);
	const bags = req.body.bags;
	for (let i = batch.bags.length - 1; i >= 0; i--) {
		if (Object.keys(bags).indexOf(batch.bags[i]._id.toString()) == -1) {
			// console.log(bags);
			// console.log(batch.bags[i]._id + " : " + bags + "  - Deleting");
			// console.log(Object.keys(bags).indexOf(batch.bags[i]._id.toString()));
			batch.bags.splice(i, 1);
			continue;
		}
		batch.bags[i].number = bags[batch.bags[i]._id].number;
		batch.bags[i].weight = bags[batch.bags[i]._id].weight;
		delete bags[batch.bags[i]._id];
	}
	for (let key in bags) {
		if (!bags.hasOwnProperty(key)) continue;
		batch.bags.push(new Bag({number: bags[key].number, weight: bags[key].weight}));
	}
	batch.save(function(err, updatedBatch) {
		if (err) {
			console.log(err);
			res.send({err: "Failed to save the updated batch."});
		} else {
			res.send(updatedBatch);
		}
	});
});

// UPDATE outputs
router.put("/:id/outputs", async function(req, res) {
	const batch = await Batch.findById(req.params.id).exec().catch((err) => {
		debug(err);
		res.send({err: "Failed to find the batch."});
	});
	if (!batch) {
		res.send({err: "Failed to find the batch."});
		return;
	}

	batch.output = req.body.output;
	const upBatch = await batch.save().catch((err) => {
		debug(err);
		res.send({err: "Failed to save the updated batch."});
	});
	res.send(upBatch);
});

module.exports = router;
