// CLIENT API ROUTES
// Route prefix: /api/clients

const express = require("express");
const router = express.Router();

const Client = require("../../models/client");

const authMW = require("../../middleware/auth");

router.use(authMW.isLoggedIn);

// CREATE API
router.post("/", function(req, res) {
	req.body.client.author = req.user;
	Client.create(req.body.client, function(err, client) {
		if (err) {
			console.log("Error creating a client through API:\n" + err);
			res.send({err: "Failed to create the new Client."});
		} else {
			res.status(201).send(client);
		}
	});
});

module.exports = router;
