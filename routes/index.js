// INDEX ROUTES
// Route prefix: "/"

const express = require("express");
const router = express.Router();

const authMW = require("../middleware/auth");

/* GET home page. */
router.get("/", authMW.isLoggedIn, function(req, res, next) {
	// res.render("index", {title: "ScanLao"});
	res.redirect("/batches");
});

module.exports = router;
