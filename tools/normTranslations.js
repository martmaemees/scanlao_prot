const jsonfile = require("jsonfile");
const fs = require("fs");

if (!process.argv[2]) {
	console.log("Need a base language to read from.");
	return;
}
if (!process.argv[3]) {
	console.log("Need a target language to write to.");
	return;
}


const readPath = "./i18n/" + process.argv[2] + ".json";
const writePath = "./i18n/" + process.argv[3] + ".json";

let readObj = jsonfile.readFileSync(readPath);
let writeObj = jsonfile.readFileSync(writePath);

console.log("Making a backup of the target file");
jsonfile.writeFileSync("./i18n/backups/" + process.argv[3] + ".json", writeObj, function(err) {
	console.log(err);
});

if (!writeObj.hasOwnProperty("TEMP_SEPARATOR_OBJ")) {
	writeObj["TEMP_SEPARATOR_OBJ"] = "===========".repeat(6);
}

Object.keys(readObj).map(function(key, index) {
	if (!writeObj.hasOwnProperty(key)) {
		writeObj[key] = readObj[key];
	}
});

jsonfile.writeFileSync(writePath, writeObj, function(err) {
	console.log(err);
});
