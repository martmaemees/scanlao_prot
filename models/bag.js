const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const bagSchema = new mongoose.Schema({
	number: Number,
	weight: Number
});

module.exports = mongoose.model("Bag", bagSchema);
