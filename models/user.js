const mongoose = require("mongoose");
const passportLocalMongoose = require("passport-local-mongoose");
const mongooseDelete = require("mongoose-delete");

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		unique: true,
		require: true
	},
	password: {
		type: String,
		select: false
	},
	email: {
		type: String,
		unique: true,
		required: true
	}
}, {timestamps: true});

userSchema.plugin(mongooseDelete, {deletedAt: true, overrideMethods: true});
userSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("User", userSchema);
