const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const clientSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	author: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User",
		required: true
	},
	phoneNumber: String,
	email: String
}, {timestamps: true});

clientSchema.plugin(mongooseDelete, {deletedAt: true, overrideMethods: true});

module.exports = mongoose.model("Client", clientSchema);
