const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const Bag = require("./bag");

const dataset = require("../config/dataset.json");

const batchSchema = new mongoose.Schema({
	client: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Client",
		required: true
	},
	author: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User",
		required: true
	},
	culture: {
		type: String,
		required: true
	},
	species: {
		type: String,
		required: true
	},
	type: {
		type: String,
		enum: dataset.batchTypes,
		required: true
	},
	bags: [Bag.schema],
	output: {
		endProduct: {
			type: Number,
			default: 0
		},
		waste: {
			type: Number,
			default: 0
		},
		oilSeed: {
			type: Number,
			default: 0
		},
		dust: {
			type: Number,
			default: 0
		},
		shells: {
			type: Number,
			default: 0
		}
	}
}, {timestamps: true});
batchSchema.plugin(mongooseDelete, {deletedAt: true, overrideMethods: true});

module.exports = mongoose.model("Batch", batchSchema);
