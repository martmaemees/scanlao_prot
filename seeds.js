const mongoose = require("mongoose");
const passport = require("passport");
const debug = require("debug")("debug");

const User = require("./models/user");
const Client = require("./models/client");
const Batch = require("./models/batch");

async function seedDb() {
	// REMOVE THIS
	if (process.env.SEED_DELETE) {
		await User.remove({});
		await Client.remove({});
		await Batch.remove({});
	}
	// -----------------

	const admin = await createAdminIfNeeded();

	try {
		if (process.env.NODE_ENV !== "production" && await Client.count({}) === 0) {
			const clients = [
				{
					name: "Client 1",
					email: "client@mail.com",
					phoneNumber: "54095748"
				},
				{
					name: "Client 2",
					email: "agroclient@mail.com",
					phoneNumber: "305-123-56"
				}
			];
			const clientIDs = [];
			for (let i = 0; i < clients.length; i++) {
				const client = new Client(clients[i]);
				client.author = admin._id;
				client.save();
				clientIDs.push(client._id);
				debug("Seeded a client");
			}

			if (await Batch.count({}) === 0) {
				const batches = [
					{
						culture: "Õlikanep",
						species: "Finola",
						type: "WHOLEGRAIN"
					},
					{
						culture: "Wheat",
						species: "White",
						type: "OILSEED"
					},
					{
						culture: 12344,
						species: 213123,
						type: "DUST"
					}
				];

				for (let i = 0; i < batches.length*20; i++) {
					const batch = new Batch(batches[i%3]);
					batch.client = clientIDs[i%2];
					batch.author = admin._id;
					batch.save();
					debug("Seeded a batch.");
				}
			}
		}
	} catch (err) {
		debug(err);
	}
}

// Passport modules don't support promises, so have to do this shit.
function createAdminIfNeeded() {
	return new Promise(async function(resolve, reject) {
		const adminCount = await User.count({username: "admin"});
		if (adminCount == 0) {
			const newUser = new User({username: "admin", email: "admin@admin.com"});
			User.register(newUser, "admin", function(err) {
				if (err) {
					reject(err);
				} else {
					debug("New admin user seeded.");
					User.findOne({username: "admin"}, function(err, user) {
						if (err) {
							reject(err);
						} else {
							resolve(user);
						}
					});
				}
			});
		} else {
			User.findOne({username: "admin"}, function(err, user) {
				if (err) {
					reject(err);
				} else {
					resolve(user);
				}
			});
		}
	});
}

module.exports = seedDb;
